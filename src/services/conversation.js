import mongoose, { Schema } from 'mongoose';

const schema = new Schema({
  name,
  cpf,
  email,
});

const model = mongoose.model('conversations', schema);

export default { model };
