const mongoose = require('mongoose');
const User = mongoose.model('User');
const jwt = require("jwt-then");

exports.login = async (req, res) => {
  const { email, cpf } = req.body;
  const user = await User.findOne({
    email,
    cpf
  });

  if (!user) throw 'Email and password do not match';

  const userInfo = {
    name: user.name,
    cpf: user.cpf,
    email: user.email,
    phone: user.phone,
    state: user.state,
    matters: user.matters,
    isEmployee: user.isEmployee
  }

  const token = await jwt.sign({ id: user.id }, process.env.SECRET);

  res.json({
    message: 'User logged in successfully',
    data: { userInfo, token }
  });
};

exports.register = async (req, res) => {
  const {
    name,
    cpf,
    email,
    phone,
    state,
    matters,
    isEmployee
  } = req.body;

  const emailRegex = /@gmail.com|@yahoo.com|@hotmail.com|@live.com/;

  if (!emailRegex.test(email)) throw 'Email is not supported';
  if (cpf.length < 11) throw 'minimal 6 characters';

  const userExists = await User.findOne({
    email,
    cpf
  });

  if (userExists) throw 'User with same email already exists';

  const user = new User({
    name,
    cpf,
    email,
    phone,
    state,
    matters,
    isEmployee
  });

  await user.save();
  const token = await jwt.sign({ id: user.id }, process.env.SECRET);
  res.json({
    message: `User [${name}] successfully created`,
    data: { user, token }
  });
};
